<?php

use Drupal\node\NodeInterface;

/**
 * @inheritdoc hook_ENTITY_TYPE_presave
 */
function backend_node_presave(NodeInterface $entity) {
  if ($entity->hasField('intro')) {
    /** @var \Drupal\paragraphs\ParagraphInterface $intro */
    $intro = $entity->get('intro')->entity;
    if ($intro) {
      $title = $intro->get('title')->value;
      if ($title !== $entity->getTitle()) {
        $entity->setTitle($title);
      }
    }
  }
}
