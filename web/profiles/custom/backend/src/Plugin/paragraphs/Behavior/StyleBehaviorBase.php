<?php

namespace Drupal\backend\Plugin\paragraphs\Behavior;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityFieldManager;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\paragraphs\Entity\ParagraphsType;
use Drupal\paragraphs\ParagraphInterface;
use Drupal\paragraphs\ParagraphsBehaviorBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a paragraphs styles behavior base class.
 */
abstract class StyleBehaviorBase extends ParagraphsBehaviorBase {

  public $options;

  public $default;

  /**
   * {@inheritdoc}
   */
  public function buildBehaviorForm(ParagraphInterface $paragraph, array &$form, FormStateInterface $form_state) {
    // Only display if this plugin is enabled and the user has the permissions.
    $form['style'] = [
      '#type' => 'select',
      '#title' => $this->t('Style'),
      '#description' => $this->t('Select a style.'),
      '#options' => $this->options,
      '#default_value' => $paragraph->getBehaviorSetting($this->getPluginId(), 'style', $this->default),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function view(array &$build, Paragraph $paragraphs_entity, EntityViewDisplayInterface $display, $view_mode) {
    // This implementation of view does nothing.
    // As this behavior affects the back end and not the display to the user.
    $debug = 1;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(Paragraph $paragraph) {
    $style = $paragraph->getBehaviorSetting($this->getPluginId(), 'style');
    return $style && isset($this->options[$style]) ? [ 'value' => $this->options[$style] ] : [];
  }

}
