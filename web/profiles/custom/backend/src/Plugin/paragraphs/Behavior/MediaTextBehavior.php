<?php

namespace Drupal\backend\Plugin\paragraphs\Behavior;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityFieldManager;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\paragraphs\Entity\ParagraphsType;
use Drupal\paragraphs\ParagraphInterface;
use Drupal\paragraphs\ParagraphsBehaviorBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a paragraphs intro behavior plugin.
 *
 * @ParagraphsBehavior(
 *   id = "media_text",
 *   label = @Translation("Media & Text"),
 *   description = @Translation("Provides selectable media & text styles."),
 *   weight = 3
 * )
 */
class MediaTextBehavior extends StyleBehaviorBase {

  public $options = [
    'big_left' => 'Big Left',
    'big_right' => 'Big Right',
    'medium_left' => 'Medium Left',
    'medium_right' => 'Medium Right',
  ];

  public $default = 'big_left';

}
