<?php

namespace Drupal\backend\Plugin\paragraphs\Behavior;


use Drupal\Component\Utility\Html;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\paragraphs\ParagraphInterface;
use Drupal\paragraphs\ParagraphsBehaviorBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Paragraphs Anchor plugin.
 *
 * @ParagraphsBehavior(
 *   id = "anchor",
 *   label = @Translation("Anchor"),
 *   description = @Translation("Allows to set ID attribute that can be used as jump position in URLs"),
 *   weight = 3
 * )
 */
class AnchorBehavior extends ParagraphsBehaviorBase {

  /**
   * Config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * ParagraphsSliderPlugin constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityFieldManagerInterface $entity_field_manager, ConfigFactoryInterface $config_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_field_manager);
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition,
      $container->get('entity_field.manager'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function view(array &$build, Paragraph $paragraph, EntityViewDisplayInterface $display, $view_mode) {
    if ($anchor = $paragraph->getBehaviorSetting($this->getPluginId(), 'anchor')) {
      $id = Html::getUniqueId($anchor);
      if (isset($build['paragraphs_title'])) {
        $build['paragraphs_title']['#attributes']['id'] = $id;
        $build['paragraphs_title']['#attributes']['class'][] = 'paragraphs-anchor-link';
      }
      elseif (isset($build['paragraphs_subtitle'])) {
        $build['paragraphs_subtitle']['#attributes']['id'] = $id;
        $build['paragraphs_subtitle']['#attributes']['class'][] = 'paragraphs-anchor-link';
      }
      else {
        $build['#attributes']['id'] = $id;
        $build['#attributes']['class'][] = 'paragraphs-anchor-link';
      }

      if ($this->configFactory->get('base.settings')->get('anchor.show_permalink')) {
        $build['#attached']['library'][] = 'base/anchor';
      }
    }

  }

  /**
   * {@inheritdoc}
   */
  public function buildBehaviorForm(ParagraphInterface $paragraph, array &$form, FormStateInterface $form_state) {
    $form['anchor'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Anchor'),
      '#description' => $this->t('Sets an ID attribute in the Paragraph so that it can be used as a jump-to link. The anchor name can contain only lowercase characters, numbers, - and _.'),
      '#default_value' => $paragraph->getBehaviorSetting($this->getPluginId(), 'anchor'),
      '#prefix' => '<div class="paragraphs-plugin-inline-container multiple-lines">',
      '#suffix' => '</div>',
      '#attributes' => ['class' => ['paragraphs-plugin-form-element']],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateBehaviorForm(ParagraphInterface $paragraph, array &$form, FormStateInterface $form_state) {
    // Verify that the machine name not only consists of valid characters.
    if (!empty($form['anchor']['#value']) && !preg_match('/^[a-z][a-z0-9_-]+$/', $form['anchor']['#value'])) {
      $form_state->setError($form, t('The anchor name can contain only lowercase characters, numbers, - and _.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitBehaviorForm(ParagraphInterface $paragraph, array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    if (!$form_state->getValue('anchor')) {
      $values['anchor'] = static::generateId($paragraph);
    }
    $paragraph->setBehaviorSettings($this->getPluginId(), $values);
  }

  /**
   * Generates an id for being used as anchor for the paragraph.
   *
   * @param ParagraphInterface $paragraph
   *   The paragraph entity.
   *
   * @return string
   *   The generated anchor.
   */
  public static function generateId(ParagraphInterface $paragraph) {
    $text = NULL;
    if ($paragraph->hasField('paragraphs_title')) {
      $text = $paragraph->get('paragraphs_title')->value;
    }
    elseif ($paragraph->hasField('paragraphs_subtitle')) {
      $text = $paragraph->get('paragraphs_subtitle')->value;
    }
//    if ($text) {
//      $clean_text = \Drupal::service('pathauto.alias_cleaner')->cleanString($text);

      // Also replace any non-alphabetic character at the beginning.
//      return preg_replace('/^[^a-z]+/', '', $clean_text);
//    }
    return '';
  }

}
