<?php

namespace Drupal\backend\Plugin\paragraphs\Behavior;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityFieldManager;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\paragraphs\Entity\ParagraphsType;
use Drupal\paragraphs\ParagraphInterface;
use Drupal\paragraphs\ParagraphsBehaviorBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a paragraphs intro behavior plugin.
 *
 * @ParagraphsBehavior(
 *   id = "quote",
 *   label = @Translation("Quote"),
 *   description = @Translation("Provides selectable quote styles."),
 *   weight = 3
 * )
 */
class QuoteBehavior extends StyleBehaviorBase {

  public $options = [
    'default' => 'Default',
    'highlight' => 'Highlight',
  ];

  public $default = 'default';

}
