<?php
namespace Deployer;

require 'recipe/drupal8.php';

set('ssh_type', 'native');
set('ssh_multiplexing', false);

set('http_user', 'psacln');
set('writable_mode', 'chmod');
set('writable_use_sudo', false);

// Project name
set('application', 'backend');

// Project repository
set('repository', getenv('BUILD_REPOSITORY'));

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', false);

// Shared files/dirs between deploys 
set('shared_files', [
  '.env'
]);
add('shared_dirs', [
  'files',
  'app/files',
]);

// Writable dirs by web server 
add('writable_dirs', [
  'files',
  'app/files',
]);

set('allow_anonymous_stats', false);

// Hosts
host(getenv('DEPLOYMENT_HOST'))
  ->user(getenv('DEPLOYMENT_USER'))
  ->set('deploy_path', getenv('DEPLOYMENT_PATH'));
    
// Tasks

task('cleanup:writable', function () {
  $releases = get('releases_list');
  $keep = get('keep_releases');
  $sudo  = get('cleanup_use_sudo') ? 'sudo' : '';
  $runOpts = [];
  if ($sudo) {
    $runOpts['tty'] = get('cleanup_tty', false);
  }

  if ($keep === -1) {
    // Keep unlimited releases.
    return;
  }

  while ($keep > 0) {
    array_shift($releases);
    --$keep;
  }

  foreach ($releases as $release) {
    run("$sudo chmod -R 777 {{deploy_path}}/releases/$release", $runOpts);
  }
  write('Cleanup preparation done!');
});

before('cleanup', 'cleanup:writable');

task('build', function () {
    run('cd {{release_path}} && build');
});

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

